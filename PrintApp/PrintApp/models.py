from django.core.files.storage import default_storage
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.template.defaultfilters import slugify
from django.core.files.images import ImageFile

from random import randint
import cv2
import numpy as np
from PIL import Image, ImageEnhance
from io import BytesIO

# Create your models here.

class Images(models.Model):
    imgId = models.AutoField(primary_key=True)
    scan = models.ImageField(upload_to='scan/')
    mask = models.ImageField(upload_to='mask/')
    # svg = models.ImageField(upload_to='svg/')
    printer = models.CharField(max_length=50, null=True)
    coordinates = models.TextField(null=True)


    def save(self, *args, **kwargs):
        img = Image.open(self.scan)
        # yellow upper/lower
        yellow_lower = np.array([255, 255, 85], dtype="uint8")
        yellow_upper = np.array([255, 255, 238], dtype="uint8")
        # grey upper/lower
        grey_lower = np.array([245, 245, 245], dtype="uint8")
        grey_upper = np.array([252, 252, 252], dtype="uint8")
        # color enhance in PIL
        img_enhanced = ImageEnhance.Color(img)
        img_enhanced = img_enhanced.enhance(2.4)
        img_enhanced2 = ImageEnhance.Sharpness(img_enhanced)
        img_enhanced2 = img_enhanced2.enhance(4.0)
        # change PIL ro numpy RGB image
        im_np = np.asarray(img_enhanced2)
        yellow_mask = cv2.inRange(im_np, yellow_lower, yellow_upper)
        grey_mask = cv2.inRange(im_np, grey_lower, grey_upper)
        mask = cv2.bitwise_or(yellow_mask, grey_mask)
        # target = cv2.bitwise_and(im_np, im_np, mask=mask)
        src = cv2.merge((mask,mask,mask))
        src[np.where((src == [255, 255, 255]).all(axis=2))] = [0, 0, 255]
        tmp = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)
        _, alpha = cv2.threshold(tmp, 0, 255, cv2.THRESH_BINARY)
        b, g, r = cv2.split(src)
        rgba = [b, g, r, alpha]
        dst = cv2.merge(rgba, 4)
        Image.fromarray(dst)

        f = BytesIO()
        Image.fromarray(dst).save(f, format="png")
        self.mask.save(self.scan.name, f, save=False)
        super().save(*args, **kwargs)

# @receiver(pre_save, sender=Images)
# def imgChange(sender, instance, **kwargs):
#     # img = default_storage.open(instance.scan)
#     img = Image.open(instance.scan)
#     yellow_lower = np.array([255, 255, 85], dtype="uint8")
#     yellow_upper = np.array([255, 255, 238], dtype="uint8")
#     # color enhance in PIL
#     img_enhanced = ImageEnhance.Color(img)
#     img_enhanced = img_enhanced.enhance(2.4)
#     img_enhanced2 = ImageEnhance.Sharpness(img_enhanced)
#     img_enhanced2 = img_enhanced2.enhance(4.0)
#     # change PIL ro numpy RGB image
#     im_np = np.asarray(img_enhanced2)
#     yellow_mask = cv2.inRange(im_np, yellow_lower, yellow_upper)
#     # grey_mask = cv2.inRange(img_hsv, grey_lower, grey_upper)
#     src = cv2.merge((yellow_mask,yellow_mask,yellow_mask))
#     src[np.where((src == [255, 255, 255]).all(axis=2))] = [0, 0, 255]
#     tmp = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)
#     _, alpha = cv2.threshold(tmp, 0, 255, cv2.THRESH_BINARY)
#     b, g, r = cv2.split(src)
#     rgba = [b, g, r, alpha]
#     dst = cv2.merge(rgba, 4)
#     instance.mask = ImageFile(dst)
# def svg_pre_save(self, *args, **kwargs):
#         # CREATING MASK
#         #
#         # yellow 2 upper/lower
#         yellow_lower = np.array([255, 255, 85], dtype="uint8")
#         yellow_upper = np.array([255, 255, 238], dtype="uint8")
#         # grey upper/lower
#         grey_lower = np.array([245, 245, 245], dtype="uint8")
#         grey_upper = np.array([252, 252, 252], dtype="uint8")
#         img = Image.open(self.scan)
#
#         img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
#         im_pil = Image.fromarray(img_rgb)
#
#         # color enhance in PIL
#         img_enhanced = ImageEnhance.Color(im_pil)
#         img_enhanced = img_enhanced.enhance(2.4)
#         img_enhanced2 = ImageEnhance.Sharpness(img_enhanced)
#         img_enhanced2 = img_enhanced2.enhance(4.0)
#
#         # change PIL ro numpy RGB image
#         im_np = np.asarray(img_enhanced2)
#         yellow_mask = cv2.inRange(im_np, yellow_lower, yellow_upper)
#         grey_mask = cv2.inRange(im_np, grey_lower, grey_upper)
#
#         # binding yellow_mask and grey_mask together
#         mask = cv2.bitwise_or(yellow_mask, grey_mask)
#         src = cv2.merge((mask, mask, mask))
#         img_gray = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)
#
#         # CREATING SVG
#         ret, thresh = cv2.threshold(img_gray, 150, 255, cv2.THRESH_BINARY)
#
#         # detect the contours on the binary image using cv2.CHAIN_APPROX_NONE
#         contours, hierarchy = cv2.findContours(image=thresh, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_NONE)
#         # draw contours on the original image
#
#         # coordinates
#         font = cv2.FONT_HERSHEY_COMPLEX
#         # array = np.array([])
#
#         # Going through every contours found in the image.
#         width = 2480
#         height = 3508
#         value = randint(0, 1000)
#         f = open('svg_mask' + str(value) + '.svg', 'w+')
#         f.write('<svg width="' + str(width) + '" height="' + str(height) + '" xmlns="http://www.w3.org/2000/svg">')
#
#         for cnt in contours:
#
#             approx = cv2.approxPolyDP(cnt, 0.009 * cv2.arcLength(cnt, True), True)
#
#             cv2.drawContours(src, [approx], 0, (0, 0, 255), 2)
#
#             n = approx.ravel()
#             i = 0
#
#             for j in n:
#                 if (i % 2 == 0):
#                     x = n[i]
#                     y = n[i + 1]
#                     print(x, y)
#                     f.write('<circle cx="' + str(x) + '" cy="' + str(y) + '" r="1" fill="red"/>')
#
#                 i = i + 1
#         f.write('</svg>')
#         f.close()
#
#         self.svg.save(self.scan.name, f, save=False)
#         super().save(*args, **kwargs)
#
#
# pre_save.connect(svg_pre_save, sender=Images)