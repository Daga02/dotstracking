from rest_framework import serializers
from PrintApp.models import Images

class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Images
        fields = ('imgId', 'scan', 'mask', 'printer', 'coordinates')