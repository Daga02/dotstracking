import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PrintsComponent } from './prints/prints.component';
import { SharedService } from './shared.service';
import { HttpClientModule } from '@angular/common/http';

import { NgxPanZoomModule } from 'ngx-panzoom';

@NgModule({
  declarations: [AppComponent, PrintsComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxPanZoomModule,
  ],
  providers: [SharedService],
  bootstrap: [AppComponent],
})
export class AppModule {}
