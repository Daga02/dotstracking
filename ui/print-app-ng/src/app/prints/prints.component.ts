import { Component, OnInit } from '@angular/core';
import { PanZoomConfig } from 'ngx-panzoom';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-prints',
  templateUrl: './prints.component.html',
  styleUrls: ['./prints.component.css'],
})
export class PrintsComponent implements OnInit {
  constructor(private service: SharedService) {}

  PrintsList: any = [];

  ngOnInit(): void {
    this.refreshImgList();
  }
  refreshImgList() {
    this.service.getImgList().subscribe((data) => {
      this.PrintsList = data;
    });
  }
  status: boolean = false;
  showMask() {
    this.status = !this.status;
  }
  myThumbnail = 'http://127.0.0.1:8000/media/scan2_6p9h789.jpg';
  myFullresImage = 'http://127.0.0.1:8000/media/scan2_6p9h789.jpg';
  scale: number = 1;

  panZoomConfig: PanZoomConfig = new PanZoomConfig();
  // default zoom 4
}
