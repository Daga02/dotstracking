import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PrintsComponent } from './prints/prints.component';

const routes: Routes = [{ path: '', component: PrintsComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
